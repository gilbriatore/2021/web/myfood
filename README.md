# MyFood I Versão Final

Versão final do  projeto MyFood desenvolvido durante as aulas de desenvolvimento web básico.

Playlist do youtube de todas as aulas: 
https://www.youtube.com/watch?v=LvzKuTgo3rg&list=PLfqtDthdyWq4FuAW4b9IzhZVDhflM1PQs


# Configuração inicial
1. Instalar no ambiente de desenvolvimento o Node.js (https://nodejs.org).
2. Fazer o checkout do projeto utilizando: git clone https://gitlab.com/gilbriatore/2021/web/myfood.git

# Para rodar o servidor de APIs (srv-apis)
1. Rodar, na pasta do servidor de APIs (srv-apis), o comando: npm install
2. Executar, na pasta do servidor, o comando: npm start
3. O servidor será iniciado em `http://localhost:8000/`. 

# Para rodar o servidor web (srv-web)
1. Rodar, na pasta do servidor web (srv-web), o comando: npm install
2. Executar, na pasta do servidor, o comando: npm start
3. O servidor será iniciado em `http://localhost:3000/`. 
